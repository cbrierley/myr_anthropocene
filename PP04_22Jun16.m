% PP04 model of ice volume (v), antarctic ice area (a) and
% atmospheric carbon dioxide concentration (c).
clear all
%---------------- Arbitrary Variable Starting Values ------------------ 
startv = 1;
starta = 1;
startc = 0;
%---------------------- PP04 PARAMETER CONSTANTS ----------------------
aa = 0.4;
bb = 0.7;
cc = 0.01;
dd = 0.27;
xx = 1.3;
yy = 0.5;
zz = 0.8;
alpha = 0.15;
beta = 0.5;
gamma = 0.5;
delta = 0.4;

%------------------------ PP04 RELAXATION TIMES ------------------------
tv = 15;
tc = 5;
ta = 12;
%------------------------------ INSOLATION ------------------------------  
            %------------ Huyber (2006) Insolation ------------
% Insolation at 65N on 21st June
% kyear = 0:1:5000;
     % [kyear,day] = meshgrid(0:1:5000,1:1:365);
% [ecc,epsilon,omega] = orbital_parameters(kyear);
     %Fsw = daily_insolation(0:1:5000,65,173,1)*864*10^-3; 
    % plot(-(0:1:5000),sum(Fsw,1));
            
% Insolation at 60S on 21st February
%kyear = 0:1:5000;
    % [kyear,day] = meshgrid(0:1:5000,1:1:365);
%[ecc,epsilon,omega] = orbital_parameters(kyear);
    %Fsw2 = daily_insolation(0:1:5000,-60,52,1)*864*10^-3; 
    % plot(-(0:1:5000),sum(Fsw2,1));

%------------------------ PP04 MODEL EQUATIONS ------------------------
dt = 1; % Time frame interval
start_date = 500; %5000ka = 5Ma - 500=0.5Myr
end_date = 0; %0ka = Present

t = linspace(start_date,end_date,1+(start_date-end_date)/dt); % Time frame

v = zeros(length(t));
a = zeros(length(t));
c = zeros(length(t));

v(1) = startv;
a(1) = starta;
c(1) = startc;

   
for tstep = 2:length(t)
    
    Fsw = daily_insolation(tstep,65,173,1)*(864*10^-9); %timestep must be inputted in daily_insolation file format
    Fsw2 = daily_insolation(tstep,-60,52,1)*(864*10^-9); %timestep must be inputted in daily_insolation file format
    
    dv = dt*(-xx * c(tstep-1) - yy * Fsw + zz - v(tstep-1)) / tv;  
    v(tstep) = v(tstep-1) + dv;
    
    da = dt* (v(tstep-1) - a(tstep-1)) / ta;
    a(tstep) = a(tstep-1) + da;
   
    
    ff = aa * v(tstep-1) - bb * a(tstep-1) - cc * Fsw2 + dd;
    
    if ff < 0
        H = 1;
    else
        H = 0;
    end
 
    dc = dt*(alpha*Fsw - beta*v(tstep-1) + gamma*H + delta - c(tstep-1)) / tc;
    c(tstep) = c(tstep-1) + dc;
     
end

% DV=max(dv);
% DA=max(da);
% DC=max(dc);
% 
% dvp = (dv/DV);
% dap = (da/DA);
% dcp = (dc/DC);

%--------------------------- PP04 OUTPUTS ---------------------------

figure(1)
subplot(3,1,1)
plot(-(t),v,'b-')
title('Global Ice Volume')
ylabel('Volume')
xlabel('Time Kyr BP')

subplot(3,1,2)
plot(-(t),a,'y-')
title('Antarctic Ice Area')
ylabel('Area')
xlabel('Time Kyr BP')

subplot(3,1,3)
plot(-(t),c,'r-')
title('Atmospheric Carbon Concentration')
ylabel('Concentration')
xlabel('Time Kyr BP')
%-------------------------------------------------------------------------
% figure(2)
% subplot(2,1,1)
% plot(t,Fsw,'r-')
% title('Insolation at 65N')
% ylabel('Insolation')
% xlabel('Time Kyr BP')
% subplot(2,1,2)
% plot(t,Fsw2,'b-')
% title('Insolation at 60S')
% ylabel('Insolation')
% xlabel('Time Kyr BP')
%-------------------------------------------------------------------------
% x1 = -(0:1:1000);
% x2 = -(1001:1:2000);
% x3 = -(2001:1:3000);
% x4 = -(3001:1:4000);
% x5 = -(4001:1:5000);
% y1 = daily_insolation(0:1:1000,65,173,1)*864*10^-9; 
% y2 = daily_insolation(1001:1:2000,65,173,1)*864*10^-3; 
% y3 = daily_insolation(2001:1:3000,65,173,1)*864*10^-3; 
% y4 = daily_insolation(3001:1:4000,65,173,1)*864*10^-3; 
% y5 = daily_insolation(4001:1:5000,65,173,1)*864*10^-3; 
% z1 = daily_insolation(0:1:1000,-60,52,1)*864*10^-3;
% z2 = daily_insolation(1001:1:2000,-60,52,1)*864*10^-3; 
% z3 = daily_insolation(2001:1:3000,-60,52,1)*864*10^-3; 
% z4 = daily_insolation(3001:1:4000,-60,52,1)*864*10^-3; 
% z5 = daily_insolation(4001:1:5000,-60,52,1)*864*10^-3; 
% figure(3)
% subplot(5,1,1)
% [ax, h1, h2] = plotyy(x1, y1, x1, z1, 'plot');
% subplot(5,1,2)
% [ax1, h3, h4] = plotyy(x2, y2, x2, z2, 'plot');
% subplot(5,1,3)
% [ax2, h5, h6] = plotyy(x3, y3, x3, z3, 'plot');
% subplot(5,1,4)
% [ax3, h7, h8] = plotyy(x4, y4, x4, z4, 'plot');
% subplot(5,1,5)
% [ax4, h9, h10] = plotyy(x5, y5, x5, z5, 'plot');
%-------------------------------------------------------------------------
% figure(4)
% plot(-t,H)
% title('Oceanic Switch, (H)')
%-------------------------------------------------------------------------
% figure(5)
% subplot(3,1,1)
% plot(t,dvp)
% title('Global Ice Volume')
% ylabel('Volume')
% xlabel('Time Kyr BP')
% 
% subplot(3,1,2)
% plot(t,dap,'y-')
% title('Antarctic Ice Area')
% ylabel('Area')
% xlabel('Time Kyr BP')
% 
% subplot(3,1,3)
% plot(-(0:1:5000),dcp,'r-')
% title('Atmospheric Carbon Concentration')
% ylabel('Concentration')
% xlabel('Time Kyr BP')

% figure(8)
% plot(-(kyear),dvp,'b-',-(0:1:5000),dap,'y-'':')
