# Myr Anthropocene #

This currently just a holding repository for relevant thoughts about whether the Anthropocene could be over 1 million years.

### Harriet's Work ###

* I had an MSc student looking at modifying the Palliard and Parrenin model.
* She did this in MATLAB and I've loaded up a couple of her scripts.
* Having said that, I'm not a fan of MATLAB

### Next Steps ###
* Engage with Michel Crucifix
* Download his iceages R package
* Start to modify fortran model itself